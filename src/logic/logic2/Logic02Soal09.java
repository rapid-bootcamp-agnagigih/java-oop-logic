package logic.logic2;

import logic.BasicLogic;

public class Logic02Soal09 extends BasicLogic {
    public Logic02Soal09(int n) {
        super(n);
    }

    public void isiArray(){
        int nilaiTengah = this.n/2;
        for (int i = 0; i < this.n; i++) {
            int initNumber = 1;
            for (int j = 0; j < this.n; j++) {
                if (j-i <=nilaiTengah && i-j <= nilaiTengah
                        && i+j >=nilaiTengah && i+j <=nilaiTengah + this.n - 1){
                    if (j < nilaiTengah) {
                        this.array[i][j] = String.valueOf(initNumber);
                        initNumber+=2;
                    }
                    else {
                        this.array[i][j] = String.valueOf(initNumber);
                        initNumber-=2;
                    }
                }
            }
        }
    }
    public void cetakArray() {
        this.isiArray();
        this.print();
    }
}
