package logic.logic2;

import logic.BasicLogic;

public class Logic02Soal07 extends BasicLogic {
    public Logic02Soal07(int n){
        super(n);
    }
    public void isiArray(){
        int[] numbers = new int[this.n];
        for (int i = 0; i < this.n; i++) {
            if (i<=1) numbers[i] =  1;
            else if (i < this.n/2+1) numbers[i] = numbers[i-1] + numbers[i-2];
            else numbers[i] = numbers[this.n - i - 1];
        }
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                if((j <= i && j >= (this.n - i - 1) && j <= n/2)
                        || (j >= i && j <= (this.n - i - 1) && j >= this.n/2)){
                    this.array[i][j] = String.valueOf(numbers[i]);
                }
                if ((j <= i && j <= (this.n - i - 1) && i <= this.n/2)
                        || (j >= i && j >= (this.n - i - 1) && i >= this.n/2)){
                    this.array[i][j] = String.valueOf(numbers[j]);
                }
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.print();
    }

}
