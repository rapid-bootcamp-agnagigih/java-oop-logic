package logic.logic2;

import logic.BasicLogic;

public class Logic02Soal03 extends BasicLogic {
    public Logic02Soal03(int n){
        super(n);
    }
    public void isiArray(){
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                if (i == j || i == this.n-1-j || i==0 || i == this.n-1 || j == 0 || j == this.n-1) {
                    this.array[i][j] = String.valueOf(j*2);
                }
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
