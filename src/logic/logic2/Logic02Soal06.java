package logic.logic2;

import logic.BasicLogic;

public class Logic02Soal06 extends BasicLogic {
    public Logic02Soal06 (int n){
        super(n);
    }
    public void isiArray(){
        int[] deret = new int[this.n];
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                deret[i] = (i <= 1) ? 1 : deret[i-1] + deret[i-2];
                if (j <= i && j >= (this.n - i - 1) || j >= i && j <= (this.n - i - 1) ){
                    this.array[i][j] = String.valueOf(deret[i]);
                }
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
