package logic.logic2;

import logic.BasicLogic;

public class Logic02Soal10 extends BasicLogic {
    public Logic02Soal10(int n){
        super(n);
    }
    public void isiArray(){
        int nilaiTengah = this.n/2;
        int[] numbers = new int[this.n];
        int initNumber = 1 + (nilaiTengah+1)*2;
        for (int i = 0; i < this.n; i++) {
            if (i <= nilaiTengah) {
                initNumber-=2;
                numbers[i] = initNumber;
            } else{
                initNumber+=2;
                numbers[i] = initNumber;
            }
        }

        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                if (j-i >= nilaiTengah ){
                    this.array[i][j] = String.valueOf(numbers[j-i]);;
                } else if (i-j >= nilaiTengah) {
                    this.array[i][j] = String.valueOf(numbers[this.n - 1 - i + j]);;
                } else if (i+j <=nilaiTengah ) {
                    this.array[i][j] = String.valueOf(numbers[j+i]);
                } else if (i+j >=nilaiTengah + this.n - 1) {
                    this.array[i][j] =String.valueOf(numbers[i+j-this.n+1]);
                }
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
