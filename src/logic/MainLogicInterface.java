package logic;

import logic.logicInterface.LogicInterface;
import logic.logicInterface.logic01Impl.*;
import logic.logicInterface.logic02Impl.*;

public class MainLogicInterface {
    public static void main(String[] args) {
        int k = 9;
        System.out.println("Logic 1\n");
        System.out.println("Soal 1");
        LogicInterface logic01Soal01 = new Logic01Soal01Impl(new BasicLogic(k));
        logic01Soal01.cetakArray();
        System.out.println("Soal 2");
        LogicInterface logic01Soal02 = new Logic01Soal02Impl(new BasicLogic(k));
        logic01Soal02.cetakArray();
        System.out.println("Soal 3");
        LogicInterface logic01Soal03 = new Logic01Soal03Impl(new BasicLogic(k));
        logic01Soal03.cetakArray();
        System.out.println("Soal 4");
        LogicInterface logic01Soal04 = new Logic01Soal04Impl(new BasicLogic(k));
        logic01Soal04.cetakArray();
        System.out.println("Soal 5");
        LogicInterface logic01Soal05 = new Logic01Soal05Impl(new BasicLogic(k));
        logic01Soal05.cetakArray();
        System.out.println("Soal 6");
        LogicInterface logic01Soal06 = new Logic01Soal06Impl(new BasicLogic(k));
        logic01Soal06.cetakArray();
        System.out.println("Soal 7");
        LogicInterface logic01Soal07 = new Logic01Soal07Impl(new BasicLogic(k));
        logic01Soal07.cetakArray();
        System.out.println("Soal 8");
        LogicInterface logic01Soal08 = new Logic01Soal08Impl(new BasicLogic(k));
        logic01Soal08.cetakArray();
        System.out.println("Soal 9");
        LogicInterface logic01Soal09 = new Logic01Soal09Impl(new BasicLogic(k));
        logic01Soal09.cetakArray();
        System.out.println("Soal 10");
        LogicInterface logic01Soal10 = new Logic01Soal10Imp(new BasicLogic(k));
        logic01Soal10.cetakArray();


        System.out.println("\nLogic 2\n");
        System.out.println("Soal 1");
        LogicInterface logic02Soal01 = new Logic02Soal01Impl(new BasicLogic(k));
        logic02Soal01.cetakArray();
        System.out.println("Soal 2");
        LogicInterface logic02Soal02 = new Logic02Soal02Impl(new BasicLogic(k));
        logic02Soal02.cetakArray();
        System.out.println("Soal 3");
        LogicInterface logic02Soal03 = new Logic02Soal03Impl(new BasicLogic(k));
        logic02Soal03.cetakArray();
        System.out.println("Soal 4");
        LogicInterface logic02Soal04 = new Logic02Soal04Impl(new BasicLogic(k));
        logic02Soal04.cetakArray();
        System.out.println("Soal 5");
        LogicInterface logic02Soal05 = new Logic02Soal05Impl(new BasicLogic(k));
        logic02Soal05.cetakArray();
        System.out.println("Soal 6");
        LogicInterface logic02Soal06 = new Logic02Soal06Impl(new BasicLogic(k));
        logic02Soal06.cetakArray();
        System.out.println("Soal 7");
        LogicInterface logic02Soal07 = new Logic02Soal07Impl(new BasicLogic(k));
        logic02Soal07.cetakArray();
        System.out.println("Soal 8");
        LogicInterface logic02Soal08 = new Logic02Soal08Impl(new BasicLogic(k));
        logic02Soal08.cetakArray();
        System.out.println("Soal 9");
        LogicInterface logic02Soal09 = new Logic02Soal09Impl(new BasicLogic(k));
        logic02Soal09.cetakArray();
        System.out.println("Soual 10");
        LogicInterface logic02Soal10 = new Logic02Soal10Impl(new BasicLogic(k));
        logic02Soal10.cetakArray();

    }
}
