package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal02 extends BasicLogic {
    public Logic01Soal02(int n){
        super(n);
    }
    public void isiArray(){
        int odd = 0;
        int even = 0;
        for (int i = 0; i < this.n; i++) {
            if (i % 2 == 0){
                even++;
                this.array[0][i] = String.valueOf(even);
            } else{
                odd+=3;
                this.array[0][i] = String.valueOf(odd);;
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
