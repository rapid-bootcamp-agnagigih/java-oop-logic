package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal05 extends BasicLogic {
    public Logic01Soal05(int n) {
        super (n);
    }
    public void isiArray(){
        int[] arrayInt = new int[this.n];
        for (int i = 0; i < this.n; i++) {
            if (i<= 2) arrayInt[i] = 1;
            else arrayInt[i] = arrayInt[i - 1] + arrayInt[i - 2] + arrayInt[i - 3];
            this.array[0][i] = String.valueOf(arrayInt[i]);
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
