package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal07 extends BasicLogic {
    public Logic01Soal07(int n){
        super(n);
    }
    public void isiArray(){
        char letter = 'A';
        for (int i = 0; i < this.n; i++) {
            this.array[0][i] = String.valueOf(letter);
            letter++;
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
