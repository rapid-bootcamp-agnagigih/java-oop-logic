package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal08 extends BasicLogic {
    public Logic01Soal08(int n) {
        super(n);
    }
    public void isiArray() {
        int initNumber = 1;
        char letter = 'A';
        for (int i = 0; i < this.n; i++) {
            if (i % 2 == 0){
                this.array[0][i] = String.valueOf(letter);
            } else this.array[0][i] = String.valueOf(initNumber);
            initNumber++;
            letter++;
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
