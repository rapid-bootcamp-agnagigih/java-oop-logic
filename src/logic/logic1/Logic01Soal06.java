package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal06 extends BasicLogic {
    public Logic01Soal06(int n) {
        super(n);
    }
    static boolean isPrime(int n) {
        // Check if number is less than
        // equal to 1
        if (n <= 1) return false;
        // Check if number is 2
        else if (n == 2) return true;
        // Check if n is a multiple of 2
        else if (n % 2 == 0) return false;
        // If not, then just check the odds
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            if (n % i == 0) return false;
        }
        return true;
    }

    public void isiArray(){
        int countPrimeNumber = 0;
        int number = 0;
        int[] primeNumber = new int[this.n];
        while (countPrimeNumber<this.n) {
            number++;
            if (isPrime(number)) {
                primeNumber[countPrimeNumber] = number;
                countPrimeNumber++;
            }
        }
        for (int i = 0; i < this.n; i++) {
            this.array[0][i] = String.valueOf(primeNumber[i]);
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
