package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal03 extends BasicLogic {
    public Logic01Soal03(int n){
        super(n);
    }
    public void isiArray(){
        for (int i = 0; i < this.n; i++) {
            this.array[0][i] = String.valueOf(i*2);
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
