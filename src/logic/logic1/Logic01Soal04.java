package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal04 extends BasicLogic {
    public Logic01Soal04(int n){
        super(n);
    }
    public void isiArray(){
        int[] arrayInt = new int[this.n];
        for (int i = 0; i < this.n; i++) {
            if (i <= 1) arrayInt[i] = 1;
            else arrayInt[i] = arrayInt[i - 1] + arrayInt[i - 2];
            this.array[0][i] = String.valueOf(arrayInt[i]);
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
