package logic.logic1;

import logic.BasicLogic;

public class Logic01Soal01 extends BasicLogic {
    public Logic01Soal01(int n) {
        super(n);
    }

    public void isiArray(){
        for (int i=0; i < this.n; i++){
            this.array[0][i] = String.valueOf(i+1);
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
