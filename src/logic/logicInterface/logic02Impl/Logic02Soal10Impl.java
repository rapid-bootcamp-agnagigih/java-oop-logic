package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal10Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal10Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int nilaiTengah = this.logic.n/2;
        int[] numbers = new int[this.logic.n];
        int initNumber = 1 + (nilaiTengah+1)*2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i <= nilaiTengah) {
                initNumber-=2;
                numbers[i] = initNumber;
            } else{
                initNumber+=2;
                numbers[i] = initNumber;
            }
        }

        for (int i = 0; i < this.logic.n; i++) {
            for (int j = 0; j < this.logic.n; j++) {
                if (j-i >= nilaiTengah ){
                    this.logic.array[i][j] = String.valueOf(numbers[j-i]);;
                } else if (i-j >= nilaiTengah) {
                    this.logic.array[i][j] = String.valueOf(numbers[this.logic.n - 1 - i + j]);;
                } else if (i+j <=nilaiTengah ) {
                    this.logic.array[i][j] = String.valueOf(numbers[j+i]);
                } else if (i+j >=nilaiTengah + this.logic.n - 1) {
                    this.logic.array[i][j] =String.valueOf(numbers[i+j-this.logic.n+1]);
                }
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
}
