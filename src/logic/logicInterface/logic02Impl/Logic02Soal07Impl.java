package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal07Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal07Impl(BasicLogic logic) {
        this.logic = logic;
    }
    public void isiArray(){
        int[] numbers = new int[this.logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i<=1) numbers[i] =  1;
            else if (i < this.logic.n/2+1) numbers[i] = numbers[i-1] + numbers[i-2];
            else numbers[i] = numbers[this.logic.n - i - 1];
        }
        for (int i = 0; i < this.logic.n; i++) {
            for (int j = 0; j < this.logic.n; j++) {
                if((j <= i && j >= (this.logic.n - i - 1) && j <= this.logic.n/2)
                        || (j >= i && j <= (this.logic.n - i - 1) && j >= this.logic.n/2)){
                    this.logic.array[i][j] = String.valueOf(numbers[i]);
                }
                if ((j <= i && j <= (this.logic.n - i - 1) && i <= this.logic.n/2)
                        || (j >= i && j >= (this.logic.n - i - 1) && i >= this.logic.n/2)){
                    this.logic.array[i][j] = String.valueOf(numbers[j]);
                }
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
}
