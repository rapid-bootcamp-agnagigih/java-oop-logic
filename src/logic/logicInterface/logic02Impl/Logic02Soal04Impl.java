package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal04Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal04Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int[] deret = new int[this.logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            for (int j = 0; j < this.logic.n; j++) {
                deret[j] = (j <= 1) ? 1 : deret[j-1] + deret[j-2];
                if (this.logic.n%2==0){
                    if (i==0 || i == (this.logic.n/2) || i == (this.logic.n/2) - 1 || i == this.logic.n-1 || j==0
                            || j == (this.logic.n/2) || j == (this.logic.n/2) - 1 || j == this.logic.n-1){
                        this.logic.array[i][j] = String.valueOf(deret[j]);
                    }
                } else{
                    if (i==0 || i == (this.logic.n/2) || i == this.logic.n-1 || j==0
                            || j == (this.logic.n/2) || j == this.logic.n-1){
                        this.logic.array[i][j] = String.valueOf(deret[j]);
                    }
                }
            }
        }
    }
    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
}
