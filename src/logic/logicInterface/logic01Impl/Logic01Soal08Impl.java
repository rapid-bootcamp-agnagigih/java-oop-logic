package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal08Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal08Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int initNumber = 1;
        char letter = 'A';
        for (int i = 0; i < this.logic.n; i++) {
            if (i % 2 == 0){
                this.logic.array[0][i] = String.valueOf(letter);
            } else this.logic.array[0][i] = String.valueOf(initNumber);
            initNumber++;
            letter++;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
