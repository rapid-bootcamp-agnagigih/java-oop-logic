package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal06Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal06Impl(BasicLogic logic) {
        this.logic = logic;
    }

    static boolean isPrime(int n) {
        // Check if number is less than
        // equal to 1
        if (n <= 1) return false;
            // Check if number is 2
        else if (n == 2) return true;
            // Check if n is a multiple of 2
        else if (n % 2 == 0) return false;
        // If not, then just check the odds
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            if (n % i == 0) return false;
        }
        return true;
    }

    public void isiArray(){
        int countPrimeNumber = 0;
        int number = 0;
        int[] primeNumber = new int[this.logic.n];
        while (countPrimeNumber<this.logic.n) {
            number++;
            if (isPrime(number)) {
                primeNumber[countPrimeNumber] = number;
                countPrimeNumber++;
            }
        }
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(primeNumber[i]);
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
