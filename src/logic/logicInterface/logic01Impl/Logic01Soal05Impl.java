package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal05Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal05Impl(BasicLogic logic) {
        this.logic = logic;
    }
    public void isiArray(){
        int[] arrayInt = new int[this.logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i<= 2) arrayInt[i] = 1;
            else arrayInt[i] = arrayInt[i - 1] + arrayInt[i - 2] + arrayInt[i - 3];
            this.logic.array[0][i] = String.valueOf(arrayInt[i]);
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
