package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal02Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal02Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int odd = 0;
        int even = 0;
        for (int i = 0; i < this.logic.n; i++) {
            if (i % 2 == 0){
                even++;
                this.logic.array[0][i] = String.valueOf(even);
            } else{
                odd+=3;
                this.logic.array[0][i] = String.valueOf(odd);;
            }
        }
    }
    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
